# About
This is a very basic demo using the ArcGIS JavaScript API to build a map, add some points, and display
in-popup charts with a link to a new page for full-page chart of the same data.
The JavaScript for them is the same and uses an in-built API via URL parameters to customize the charts
depending on where they are shown.

# How to Use
1. Download or clone this repo.
2. Open index.HTML

# Development
This application is written in simple JavaScript ES6 using the ArcGIS JavaScript API.  Modifications to the maps should
be done in the js/map_with_timeseries.js.  Changes to the charts should be in the js/charts.js.  You can style the HTML
to your needs; these are just the bare-bones for demonstration.
The data to chart is in the ./assets/vega-data.JSON file.  For demonstration, is it configured for a simple line chart
using the [vega API](https://vega.github.io/vega/).

### Debugging
If you do not run this on a server, you will get a cross-browser orgin request error.  Please open this using
and IDE or load to a server to avoid this issue.

# URL Parameters
* id (string) = Location ID to chart (demo only supports 1 or 2, add more data to JSON for additional IDs)
* w (int) = width of the chart
* h (int) = height of the chart
* t (boolean) = show the title or not

# Author
R. Dylan Walker 