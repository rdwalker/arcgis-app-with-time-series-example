let view;
let urlParams = new URLSearchParams(window.location.search);
let id = urlParams.get("id") ? urlParams.get("id") : 1;
let h = urlParams.get("h") ? urlParams.get("h") : 400;
let w = urlParams.get("w") ? urlParams.get("w") : 1000;
let showTitle = urlParams.get("t") ? urlParams.get("t") : true;
console.log(showTitle);
showTitle = showTitle === "false" ? false : true ;
console.log(showTitle);
vega.loader()
    .load('./assets/vega-data.JSON')
    .then(function(data) {
        // Modify JSON with id
        let re = /OID/gi;
        data = data.replace(re, id);
        data = JSON.parse(data);
        data.width = w;
        data.height = h;
        document.getElementById("title").innerText = `Chart for Location ${id}`;
        if (!showTitle) {
            document.getElementById("title").style.display = "none";
        }
        render(data);
    });

function render(spec) {
    view = new vega.View(vega.parse(spec))
        .renderer('svg')  // set renderer (canvas or svg)
        .initialize('#chart') // initialize view within parent DOM container
        .hover()             // enable hover encode set processing
        .run();
}

