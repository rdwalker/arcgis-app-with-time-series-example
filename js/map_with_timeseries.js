/**
 * Script to generate the elements and maps necessary for building the Site Location Map Creator
 * R. Dylan Walker
 * dwalker@geosyntec.com
 * 15 January 2018
 */

const chart_url = "./chart.html";

// Load resources
require([
    "esri/Map",
    "esri/views/MapView",
    "esri/Graphic",
    "esri/layers/FeatureLayer",
    "esri/layers/support/LabelClass",
    "esri/widgets/ScaleBar",
    "esri/widgets/Legend",
    "esri/widgets/Popup",
    "dojo/domReady!"], function (Map, MapView, Graphic, FeatureLayer, LabelClass, ScaleBar, Legend, Popup) {


        
    /**
     * Function to create a view in the given container of the give map with center coordinates and scale
     * @param container DOM element to put the view into
     * @param map ESRI Map to use for the view
     * @param center Center of the map given as a two value list; [long, lat]
     * @param scale Scale of the map given as a value (e.g., 1:24000 would be scale = 24000)
     * @param zoom Zoom level of the map given as integer between 1 and 12
     * @returns {object} ESRI MapView
     */
    const createView = (container, map, center, scale = null, zoom = null) => {
        let view = new MapView({
            container: container,
            map: map,
            center: center
        });
        if (scale) {
            view.scale = scale
        }
        if (zoom) {
            view.zoom = zoom
        }
        //Check error on view
        view.when(() => {
        }, (error) => {
            console.log("The view resources failed to load: ", error)
        });
        return view;
    };

    /**
     * Initialization function
     */
    const init = () => {
        // Create a map used for selection of location
        let map = new Map({
            basemap: 'topo'
        });
        let initial_lat = -85;
        let initial_long = 38;
        // configuration view of the map
        let mview = createView("map", map, [initial_lat, initial_long], 12000);

        // Remove loading modal after views have finished
        mview.when(() => {
            // add scalebar
            add_scalebar(mview);
            //Add a bunch of points
            let num_points = 2;
            for (let i=1; i<=num_points; i++) {
                add_point(map,
                    ((initial_long+Math.random()/100)+(initial_long-Math.random()/100))/2,
                    ((initial_lat+Math.random()/100)+(initial_lat-Math.random()/100))/2,
                    i)
            }
            document.getElementsByTagName("body")[0].setAttribute("class", "");
        });


    };

    const add_point = (map, lat, long, id) => {
        /**
         * Adds a new point to the given map at the given latitude and longtitude
         * Also pans and zooms the map to the given point at scale 1:24000         *
         */
        // Remove any existing layers
        map.removeAll();

        // Create point geometry
        let point = {
            type: "point",
            longitude: long,
            latitude: lat
        };

        // Create symbol
        let pointSymbol = {
            type: "simple-marker",
            style: "circle",
            color: [255, 0, 0, 1]
        };

        // Create graphic to add to map
        let ptPromise = new Promise((resolve) => {
            let pointGraphic = new Graphic({
                geometry: point,
                symbol: pointSymbol,
                attributes: {
                    ObjectID: id  ,
                    ChartURL: chart_url + `?id=${id}`,
                    miniChartURL: chart_url + `?id=${id}&h=100&w=250&t=false`,
                }
            });
            resolve(pointGraphic)
        });

        // Put graphic into map layer
        let ptRenderer = {
            type: "simple",
            symbol: pointSymbol,
            label: "A Point"
        };
        // Minimum required fields for ESRI point to work
        let fields = [
            {name: "ObjectID", alias: "ObjectID", type: "oid"},
            {name: "ChartURL", alias: "Chart URL", type: "string"}
        ];

        //Popup Template
        let template = {
            title: "Location {ObjectID}",
            content: "<iframe src='{miniChartURL}' height='180' width='350'></iframe><br><a href='{ChartURL}' target='_blank'>Enlarge</a>"
        };

        // Create layer from graphic and pan to point
        ptPromise.then((pointGraphic) => {
            let layer = new FeatureLayer({
                source: [pointGraphic],
                geometryType: "point",
                spatialReference: {wkid: 4326},
                fields: fields,
                objectIdField: "ObjectID",
                renderer: ptRenderer,
                popupTemplate: template
            });
            map.add(layer);

        });
    };

    const add_scalebar = (view) => {
        /**
         * Creates an ESRI Scalebar in the given view         *
         */
        let scalebar = new ScaleBar({
            view: view,
            container: 'present_scalebar',
            style: "ruler",
            units: "feet"
        });
        view.ui.add(scalebar, {
            position: "bottom-left"
        });
    };

    // Show loading modal
    document.getElementsByTagName("body")[0].setAttribute("class", "loading");

    // Start
    init();

});